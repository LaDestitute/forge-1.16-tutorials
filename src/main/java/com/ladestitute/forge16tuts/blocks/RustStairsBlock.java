package com.ladestitute.forge16tuts.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.block.StairsBlock;

public class RustStairsBlock extends StairsBlock {
    public RustStairsBlock(BlockState state, Properties properties) {
        super(state, properties);
    }
}
