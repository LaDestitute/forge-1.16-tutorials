package com.ladestitute.forge16tuts.client.render;

import com.ladestitute.forge16tuts.Forge16TutsMain;
import com.ladestitute.forge16tuts.entities.EntityPoisonArrow;
import net.minecraft.client.renderer.entity.ArrowRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.util.ResourceLocation;

public class PoisonArrowRender extends ArrowRenderer<EntityPoisonArrow> {

    protected static final ResourceLocation TEXTURE = new ResourceLocation(Forge16TutsMain.MOD_ID, "textures/entity/arrows/poison_arrow.png");

    public PoisonArrowRender(EntityRendererManager renderManagerIn) {
        super(renderManagerIn);
    }

    @Override
    public ResourceLocation getTextureLocation(EntityPoisonArrow entity) {
        return TEXTURE;
    }


}

