package com.ladestitute.forge16tuts.items;

import com.ladestitute.forge16tuts.util.Forge16TutsKeyboardUtil;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.TridentItem;
import net.minecraft.item.UseAction;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public class LanzaniteItem extends Item {
    public LanzaniteItem(Properties properties)
    {
        super(properties.stacksTo(64));
    }

    @Override
    public void appendHoverText(ItemStack stack, World worldIn, List<ITextComponent> tooltip, ITooltipFlag flagIn)
    {
        super.appendHoverText(stack, worldIn, tooltip, flagIn);
        if(Forge16TutsKeyboardUtil.isHoldingLeftShift())
        {
            tooltip.add(new StringTextComponent(TextFormatting.GOLD+"A shiny rock. Used to make weapons and tools."));
        }
        else
        {
            tooltip.add(new StringTextComponent("Hold Left Shift to see more info!"));

        }
    }
}
