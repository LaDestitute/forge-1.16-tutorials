package com.ladestitute.forge16tuts.registries;

import com.ladestitute.forge16tuts.Forge16TutsMain;
import com.ladestitute.forge16tuts.blocks.*;
import net.minecraft.block.Block;
import net.minecraft.block.SlabBlock;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class BlockInit {
    public static final DeferredRegister<Block> BLOCKS = DeferredRegister.create(ForgeRegistries.BLOCKS,
            Forge16TutsMain.MOD_ID);

    public static final RegistryObject<Block> RUST = BLOCKS.register("rust",
            () -> new RustBlock(PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> POINTER_BLOCK = BLOCKS.register("pointer_block",
            () -> new PointerBlock(PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> RUST_PLATE = BLOCKS.register("rust_plate",
            () -> new RustPlateBlock(PropertiesInit.RUST_PLATE));

    public static final RegistryObject<Block> RUST_BUTTON = BLOCKS.register("rust_button",
            () -> new RustButtonBlock(PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> RUST_STAIRS = BLOCKS.register("rust_stairs",
            () -> new RustStairsBlock(RUST.get().defaultBlockState(), PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> RUST_WALL = BLOCKS.register("rust_wall",
            () -> new RustWallBlock(PropertiesInit.WRECKAGE));

    public static final RegistryObject<Block> RUST_SLAB = BLOCKS.register("rust_slab", () ->
            new RustSlabBlock(PropertiesInit.WRECKAGE));
}

