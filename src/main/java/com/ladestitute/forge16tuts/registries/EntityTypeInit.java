package com.ladestitute.forge16tuts.registries;

import com.ladestitute.forge16tuts.Forge16TutsMain;
import com.ladestitute.forge16tuts.entities.EntityPoisonArrow;
import net.minecraft.entity.EntityClassification;
import net.minecraft.entity.EntityType;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

public class EntityTypeInit {
    public static final DeferredRegister<EntityType<?>> ENTITIES = DeferredRegister.create(ForgeRegistries.ENTITIES, Forge16TutsMain.MOD_ID);

    public static final RegistryObject<EntityType<EntityPoisonArrow>> POISON_ARROW = ENTITIES.register("poison_arrow",
            () -> EntityType.Builder.<EntityPoisonArrow>of(EntityPoisonArrow::new, EntityClassification.MISC).sized(0.5F, 0.5F)
                    .build(new ResourceLocation(Forge16TutsMain.MOD_ID, "textures/entity/arrows").toString()));
}
