package com.ladestitute.forge16tuts.registries;

import com.ladestitute.forge16tuts.Forge16TutsMain;
import com.ladestitute.forge16tuts.items.*;
import com.ladestitute.forge16tuts.util.ItemToolTiers;
import net.minecraft.item.*;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;

import java.util.function.Supplier;

public class ItemInit
{
    public static final DeferredRegister<Item> ITEMS = DeferredRegister.create(ForgeRegistries.ITEMS,
            Forge16TutsMain.MOD_ID);

    public static final RegistryObject<Item> LANZANITE = ITEMS.register("lanzanite",
            () -> new LanzaniteItem(new Item.Properties().tab(Forge16TutsMain.RESOURCES)));

    public static final RegistryObject<Item> FRIED_EGG = ITEMS.register("fried_egg",
            () -> new FriedEggItem(new Item.Properties().food(new Food.Builder()
                    .nutrition(8).effect(() -> new EffectInstance(Effects.HARM, 20, 1), 1F)
                    .saturationMod(0.7f).alwaysEat()
                    .build()).tab(Forge16TutsMain.RESOURCES)));

    public static final RegistryObject<PickaxeItem> LANZANITE_PICKAXE = ITEMS.register("lanzanite_pickaxe",
            () -> new LanzanitePickaxeItem(ItemToolTiers.LANZANITE, 1, -2.8f,
                    new Item.Properties().tab(Forge16TutsMain.RESOURCES)));

    public static final RegistryObject<LanzaniteMultiToolItem> LANZANITE_MULTITOOL = ITEMS.register("lanzanite_multitool",
            () -> new LanzaniteMultiToolItem(ItemToolTiers.LANZANITE, 3, -3.0f,
                    new Item.Properties().tab(Forge16TutsMain.RESOURCES)));

    //Remember to set custom properties for your bows such as durability here
    public static final RegistryObject<BowItem> METAL_BOW = ITEMS.register("metal_bow",
            () -> new MetalBowItem(new Item.Properties().stacksTo(1).durability(700).tab(Forge16TutsMain.RESOURCES)));

    public static final RegistryObject<Item> POISON_ARROW = ITEMS.register("poison_arrow",
            () -> new PoisonArrowItem(new Item.Properties().tab(Forge16TutsMain.RESOURCES)));

        public static final RegistryObject<Item> BUBBLEGLOOP_DISC = ITEMS.register("bubblegloop_disc", () -> new MusicDiscItem(1,
            SoundInit.BUBBLEGLOOP_DISC_LAZY.get(), new Item.Properties().stacksTo(1).tab(Forge16TutsMain.RESOURCES).rarity(Rarity.RARE)));
}





